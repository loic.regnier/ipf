(* ------------------------------- QUESTION 1 ------------------------------- *)
let () = assert (word "abcdef" = L("abcdef",6));;
let () = assert (word "" = L("",0));;

let () = assert (sb_length arbre_test = 9);;
let () =  assert (sb_length (L("abcdef",6)) = 6);;

let () = assert (concat (L("TE",2)) (L("ST",2)) = N (L ("TE", 2), 4, L ("ST", 2)));;
let () = assert (concat (L("BIE",3)) (N(L("NV",2),6,N(L("ENU",3),4,L("E",1))))
= N (L ("BIE", 3), 9, N (L ("NV", 2), 6, N (L ("ENU", 3), 4, L ("E", 1)))));;


(* ------------------------------- QUESTION 2 ------------------------------- *)
let () = assert(char_at arbre_test 0 = 'a');;
let () = assert(char_at arbre_test 7 = 's');;


(* ------------------------------- QUESTION 3 ------------------------------- *)
let () = assert(sub_string 2 3 (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2)))))
                  = N (L ("TT", 2), 3, L ("A", 1)))
;;
let () = assert (sub_string 1 3 (L("abcd",4)) = L ("bcd", 3));;
let () = assert (sub_string 3 6 arbre_test 
                  = N (L ("re", 2), 6, N (L ("tes", 3), 4, L ("t", 1))))
;;


(* ------------------------------- QUESTION 4 ------------------------------- *)
let () = assert(cost (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2))))) = 16);;
let () = assert(cost arbre_test = 22);;


(* ------------------------------- QUESTION 6 ------------------------------- *)
let () = assert (list_of_string (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2)))))
            = ["G";"ATT";"A";"CA"])
;;


(* ------------------------------- QUESTION 7 ------------------------------- *)
let ech2 = echantillon 100 2 in stats ech2;;
let ech4 = echantillon 100 4 in stats ech4;;
let ech5 = echantillon 100 5 in stats ech5;;
let ech7 = echantillon 100 7 in stats ech7;;
let ech10 = echantillon 100 10 in stats ech10;;
let ech15 = echantillon 100 15 in stats ech15;;