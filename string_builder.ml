
(* ------------------------------- QUESTION 1 ------------------------------- *)


(* Type string_builder : L pour Leaf et N pour Node*)
type string_builder = 
| L of string * int (*l'entier est la longueur de la chaîne de caractères*)
| N of string_builder * int * string_builder ;; 
(*l'entier est la somme des longueurs des 2 string_builders*)

(* Exemple :

arbre_test :
              9
          /       \ 
        3           6      
    /    \        /     \
"a",1  "rb",2   "re",2   4
                        /  \  
                  "tes",3   "t",1
*)
let arbre_test = N( N( L("a",1), 3, L("rb",2) ),
                  9,N( L("re",2), 6, 
                                    N(L("tes",3), 4, L("t",1))))
;;


(**
* [word s] renvoie la feuille contenant la chaîne de caractères s
*
* s est une chaîne de caractères
*
* Test :    word "abcdef" ==> L("abcdef",6)
*           word "" ==> L("",0)
*)
let word s = L (s , String.length s)
;;

let () = assert (word "abcdef" = L("abcdef",6));;
let () = assert (word "" = L("",0));;


(**
* [sb_length sb] renvoie la longueur de la chaîne de caractères associée à sb
*
* sb est un string_builder
*
* Test :    sb_length arbre_test ==> 9
*           sb_length (L("abcdef",6)) ==> 6
*)
let sb_length sb = match sb with
| L(_,n) -> n
| N(_,n,_) -> n
;;

let () = assert (sb_length arbre_test = 9);;
let () =  assert (sb_length (L("abcdef",6)) = 6);;


(**
* [get_node l1 l2] renvoie la somme de la longueur de l1 et de l2
*
* l1 et l2 des string_builders
*
* Fonction utile pour obtenir le noeud composé des feuilles l1 et l2:
*     N(l1, (get_node l1 l2), l2)
*)
let get_node l1 l2 = sb_length l1 + sb_length l2;;

(**
* [concat sb1 sb2] renvoie la concaténation de sb1 et sb2
*
* sb1 et sb2 des string_builders
*
* La concaténation de 2 arbres sb1 et sb2 se fait en créant un noeud dont
* les feuilles sont sb1 et sb2
*
* Tests :   concat (L("TE",2)) (L("ST",2)) ==> N (L ("TE", 2), 4, L ("ST", 2))
*           concat (L("BIE",3)) (N(L("NV",2),6,N(L("ENU",3),4,L("E",1))))
      ==> N (L ("BIE", 3), 9, N (L ("NV", 2), 6, N (L ("ENU", 3), 4, L ("E", 1))));;
*)
let concat sb1 sb2 = N (sb1 , get_node sb1 sb2 , sb2)
;;

let () = assert (concat (L("TE",2)) (L("ST",2)) = N (L ("TE", 2), 4, L ("ST", 2)));;
let () = assert (concat (L("BIE",3)) (N(L("NV",2),6,N(L("ENU",3),4,L("E",1))))
= N (L ("BIE", 3), 9, N (L ("NV", 2), 6, N (L ("ENU", 3), 4, L ("E", 1)))));;




(* ------------------------------- QUESTION 2 ------------------------------- *)

(**
* [char_at sb i] renvoie le ième caractère de la chaîne associée à sb
*
* i un entier, i>=0
* sb un string_builder
*
* Renvoie une erreur si i est plus (ou moins) grand que la longueur de la chaîne
*)
let rec char_at sb i = match sb with
| L(s,n) -> String.get s i
| N(l1,n,l2) -> let k=sb_length l1 in
          if i<k then char_at l1 i 
          else char_at l2 (i-k)
;;

let () = assert(char_at arbre_test 0 = 'a');;
let () = assert(char_at arbre_test 7 = 's');;



(* ------------------------------- QUESTION 3 ------------------------------- *)

(**
* [sub_string i m sb] renvoie le string_builder associé à la sous-chaîne 
* de longueur m à partir du caractère d'indice i .
* On utilise au maximum les mêmes feuilles que sb
*
* sb un string_builder
* i,m entiers tels que : 0 <= i < i+m <= n
*
* Des erreurs sont renvoyées i les indices i ou m sont incorrects
*
* Test:
*    sb                             sub_string 2 3 sb
* "GATTACA"                              "TTA"
*
*    7                                     3
*  /   \                                  /  \ 
*G,1     6                             TT,2  A,1
*      /   \                                 
*   ATT,3   3
*          /  \
*        A,1   CA,2
*
* 
*     sub_string 1 3 (L("abcd",4)) ==> L ("bcd", 3)
*     sub_string 3 6 arbre_test ==> la chaîne associée est "retest"
*                       en utilisant les feuilles:   - L("re",2)
*                                                    - L("tes",3)
*                                                    - L("t",1)
*)
let rec sub_string i m sb = match sb with
|L(l,n) -> L((String.sub l i m),m)
|N(l1,n,l2) -> let len1 = sb_length l1 in
            if i+m<=len1 then sub_string i m l1
      else  if i>=len1 then sub_string (i-len1) m l2
      else  N(sub_string i (len1-i) l1,m,sub_string 0 (m-len1+i) l2)
;;


let () = assert(sub_string 2 3 (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2)))))
                  = N (L ("TT", 2), 3, L ("A", 1)))
;;
let () = assert (sub_string 1 3 (L("abcd",4)) = L ("bcd", 3));;
let () = assert (sub_string 3 6 arbre_test 
                  = N (L ("re", 2), 6, N (L ("tes", 3), 4, L ("t", 1))))
;;

(* ------------------------------- QUESTION 4 ------------------------------- *)


(**
* Fonction auxiliaire de cost
* [cost_aux depth sb] Renvoie le coût de sb sachant que les fils de sb,
* s'ils'existent, se trouvent à la profondeur depth
*
* depth entier positif: depth >= 0
* sb string_builder
*)
let rec cost_aux depth sb = match sb with
|L(_,_) -> 0
|N(L(_,k1),_,L(_,k2)) -> depth*k1 + depth*k2
|N(L(_,k),_,sb2) -> depth*k + cost_aux (depth+1) sb2
|N(sb1,_,L(_,k)) -> (cost_aux (depth+1) sb1) + depth*k
|N(sb1,_,sb2) -> (cost_aux (depth+1) sb1) + (cost_aux (depth+1) sb2)
;;

(**
* [cost sb] Renvoie le coût de sb
* 
* sb string_builder
*)
let cost sb = cost_aux 1 sb;;


let () = assert(cost (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2))))) = 16);;
let () = assert(cost arbre_test = 22);;



(* ------------------------------- QUESTION 5 ------------------------------- *)

(**
* [random_char c] Renvoie une lettre au hasard (parmi a-z ou A-Z)
*
* c de type quelconque
*)
let random_char c = let r=Random.int 26 in
if Random.bool () then Char.chr (r+97) else Char.chr (r+65)
;;

(**
* [random_word_len len] renvoie une chaîne de caractères de longueur len
* avec des caractères aléatoires parmi a-z ou A-Z
*
* len entier
*
* On créer une chaîne de longueur len, puis on modifie tous les caractères 
* de manière alétoire
*)
let random_word_len len = let s=String.make len 'a' in 
String.map random_char s
;;

(**
* [random_word ()] renvoie une chaîne de caractères de longueur 10 ou moins
* contenant des caractères aléatoire parmi a-z ou A-Z
*)
let random_word () = random_word_len (1+Random.int 10);; 


(**
* [random_string depth] renvoie un string_builder 'aléatoire' (définition du
* terme dans le dossier du projet) de profondeur depth
*
* depth entier positif: depth >= 0
*
* Remarque: un string_builder de profondeur 0 est une feuille
*)
let rec random_string depth = match depth with
|0 -> let rw=random_word () in L(rw,String.length rw)
|1 -> let rw1=random_word ()     and rw2=random_word ()     in
      let len1=String.length rw1 and len2=String.length rw2 in
                  N(L(rw1,len1) , len1+len2 , L(rw2,len2))
  
|_ -> let d1,d2 = if Random.bool () then (depth-1,Random.int depth)
                                    else (Random.int depth,depth-1) in
      let sb1=random_string d1 and sb2=random_string d2 in
                  N(sb1, get_node sb1 sb2 ,sb2)
;;



(* ------------------------------- QUESTION 6 ------------------------------- *)

(**
* [list_of_string sb] renvoie la liste des chaînes de caractères 
* du string_builder sb
*
* sb string_builder
*
* Tests:    list_of_string (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2)))))
            ==> ["G";"ATT";"A";"CA"]

*)
let rec list_of_string sb = match sb with
|L(l,n) -> [l]
|N(sb1,n,sb2) ->  (list_of_string sb1)@(list_of_string sb2)
;;

let () = assert (list_of_string (N(L("G",1),7,N(L("ATT",3),6,N(L("A",1),3,L("CA",2)))))
            = ["G";"ATT";"A";"CA"])
;;



(* ------------------------------- QUESTION 7 ------------------------------- *)

(**
* [list_of_leaves sb] renvoie la liste des feuilles de sb
*
* sb string_builder
*)
let rec list_of_leaves sb = match sb with
|L(l,k) -> [L(l,k)]
|N(sb1,n,sb2) -> (list_of_leaves sb1) @ (list_of_leaves sb2)
;;


(**
* [minimum_cost lst] renvoie le mimimum des coûts obtenu lors de la concaténation
* de 2 éléments successifs de lst
*
* Cette fonction permet de déterminer quels éléments concaténer dans l'algorithme
*
* lst liste de string_builders
*)
let rec minimum_cost lst = match lst with
|[] -> failwith "Error: minimum_cost: Wrong argument"
|[lf] -> cost lf
|[lf1;lf2] -> cost (concat lf1 lf2)
|lf1::lf2::lst2 -> min (cost (concat lf1 lf2)) (minimum_cost (lf2::lst2))
;; 

(**
* [concat_mincost lst mincost] renvoie la liste lst après avoir concaténé les 2 éléments successifs
* dont la concaténation avait le coût mincost
*
* lst : liste de string_builders
* mincost : entier, coût minimum de concaténation de 2 éléments successifs de lst
*)
let rec concat_mincost lst mincost = match lst with
|[] |[_]-> failwith "Error: aux_balance: Wrong argument"
|[a;b] -> [concat a b]
|a::b::lst2 -> let conc=concat a b in 
      if cost conc = mincost then conc::lst2
      else a::(concat_mincost (b::lst2) mincost)
;;


(**
* [aux_balance lst mincost] renvoie le string_builder obtenu après 
* toutes les concaténations de coût le plus faible à chaque fois
*
* lst : liste de string_builders
* mincost : entier, coût minimum de concaténation de 2 éléments successifs de lst
*)
let rec aux_balance lst mincost = match lst with
|[] -> failwith "Error: aux_balance: Wrong argument"
|[sb] -> sb 
|[lf1;lf2] -> concat lf1 lf2
|_ -> let newlst = concat_mincost lst mincost in
      aux_balance newlst (minimum_cost newlst)
;;

(**
* [balance sb] renvoie le string_builder obtenu après avoir équilibré sb
* selon l'algorithme de l'énoncé
*
* sb : le string_builder à équilibrer
*)
let balance sb = match sb with
|L(_,_) -> sb
|_ -> let lstleaves = list_of_leaves sb in
      aux_balance lstleaves (minimum_cost lstleaves)
;;



(* ------------------------------- QUESTION 8 ------------------------------- *)

(**
* [random_list_string n] renvoie une liste de n string_builders aléatoires
* de profondeur depth
*
* n un entier >= 0
* depth entier >= 0
*)
let rec random_list_string n depth = if n=0 then []
else (random_string depth)::random_list_string (n-1) depth
;;

(**
* [diff_list l1 l2] renvoie la liste des différences des éléments de l1 par 
* les éléments de l2
*
* l1,l2 listes d'entiers de même taille
*)
let rec diff_list l1 l2 = match l1,l2 with
|[],[] -> []
|a::l1',b::l2' -> (a-b)::diff_list l1' l2'
|_-> failwith "Error: Wrong argument"
;;

(**
* [echantillon n] renvoie un échantillon des gains de la fonction balance 
* sur une liste aléatoire de string_builders de profondeurs depth
*
* n >= 1 taille de l'échantillon
* depth >= 0 prodondeur des string_builders
*)
let echantillon n depth = let l = random_list_string n depth in 
                  let bl = List.map balance l in
                  diff_list (List.map cost l) (List.map cost bl)
;;



let ech = echantillon 50 10;;

(**
* Fonction utilisée en argument de List.sort pour trier la liste
* renvoie 0 si a=b, 1 si a>b, -1 si a<b
*)
let f a b = if a=b then 0 else if a>b then 1 else -1;;

(**
* [stats ech] renvoie le quadruplé minmum, maximum, moyenne, médiane
* de l'échantillon ech
*
* ech est une liste d'entiers non vide
*
* utilise la fonction intermédiaire 'aux' définie dans la fonction
* Renvoie une erreur si l'échantillon en argument est vide
*)
let stats ech = if ech=[] then failwith "Wrong argument" else
      (*on trie l'échantillon dans l'ordre croissant*)
      let sort_ech = List.sort f ech in 
      let n=List.length sort_ech in let indice_med = (n+1)/2-1 in

      (**
      * [aux (mn,mx,sum,med) lst k] retourne le quadruplé : (minimum, maximum, moyenne, mediane) 
      * en parcourant la liste qu'une seule fois

      * mn : minimum
      * mx : maximum
      * sum : somme des éléments de la liste
      * med : valeur de la médiane de la liste
      * lst : la liste en question
      * k : indice de l'élément parcouru
      *)
      let rec aux (mn,mx,sum,med) lst k = match lst with
      |[] -> (mn,mx,(float_of_int sum)/.(float_of_int n),med)
      |a::lst2 -> aux ((min a mn),(max a mx),(sum+a),(if k=indice_med then a else med))
                        lst2 (k+1)
      in


      match sort_ech with
      |[] -> (0,0,0.,0) (*sort_ech ne peut pas être vide car ech est non vide*)
      |a::lst -> aux (a,a,a,a) lst 1 (*on utilise la fonction auxilaire en initialisant 
      toutes les valeurs par la valeur du premier élément*)
;;


(* Tests de la fonction stats avec plusieurs échantillons 

On prend des échantillons de taille 100 pour toutes les profondeurs
Profondeurs testées : 2, 4, 5, 7, 10, 15
*)
let ech2 = echantillon 100 2 in stats ech2;;
let ech4 = echantillon 100 4 in stats ech4;;
let ech5 = echantillon 100 5 in stats ech5;;
let ech7 = echantillon 100 7 in stats ech7;;
let ech10 = echantillon 100 10 in stats ech10;;
let ech15 = echantillon 100 15 in stats ech15;;